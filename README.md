# emux

emux (Encrypted Multiplexer) is a multiplexing library for Golang
based on Yamux. It relies on an underlying connection to provide
reliability and ordering, such as TCP or Unix domain sockets, and
provides stream-oriented multiplexing. It is inspired by SPDY but
is not interoperable with it.

emux features include:

* Bi-directional streams
  * Streams can be opened by either client or server
  * Useful for NAT traversal
  * Server-side push support
* Flow control
  * Avoid starvation
  * Back-pressure to prevent overwhelming a receiver
* Keep Alives
  * Enables persistent connections over a load balancer
* Efficient
  * Enables thousands of logical streams with low overhead

## Documentation

For complete documentation, see Yamux doc: [Godoc](http://godoc.org/github.com/hashicorp/yamux).

If you want use encryption, just set `emux.Config.CipherType` and `emux.Config.Password`

Available `CipherType` content:

* Empty String (Default, No Encryption)
* aes-128-gcm
* aes-192-gcm
* aes-256-gcm

emux has Pool object to provide connection pool feature.

`emux.NewPool(remoteAddr string, config *emux.Config, size int) (*emux.Pool, error)`

Create new connection pool.

`Pool.Size() int`

Get Pool size.

`Pool.TakeSession() (*emux.Session, error)`

Get a Session from Pool.

`Pool.Open() (*emux.Stream, error)`

`Pool.OpenStream() (*emux.Stream, error)`

Open a Stream from Pool.


## Specification

The full specification for emux is provided in the `spec.md` file.
It can be used as a guide to implementors of interoperable libraries.

## Usage

Using emux is remarkably simple:

```go

func client() {
    // Get a TCP connection
    conn, err := net.Dial(...)
    if err != nil {
        panic(err)
    }

    // Setup client side of emux
    session, err := emux.Client(conn, nil)
    if err != nil {
        panic(err)
    }

    // Open a new stream
    stream, err := session.Open()
    if err != nil {
        panic(err)
    }

    // Stream implements net.Conn
    stream.Write([]byte("ping"))
}

func server() {
    // Accept a TCP connection
    conn, err := listener.Accept()
    if err != nil {
        panic(err)
    }

    // Setup server side of emux
    session, err := emux.Server(conn, nil)
    if err != nil {
        panic(err)
    }

    // Accept a stream
    stream, err := session.Accept()
    if err != nil {
        panic(err)
    }

    // Listen for a message
    buf := make([]byte, 4)
    stream.Read(buf)
}

```

For encryption

```go
func client() {
    // Get a TCP connection
    conn, err := net.Dial(...)
    if err != nil {
        panic(err)
    }
    // Setup client side of emux
    cfg := emux.DefaultConfig()
    cfg.CipherType = "aes-128-gcm"
    cfg.Password = "password"
    session, err := emux.Client(conn, cfg)
    if err != nil {
        panic(err)
    }

    // Open a new stream
    stream, err := session.Open()
    if err != nil {
        panic(err)
    }

    // Stream implements net.Conn
    stream.Write([]byte("ping"))
}

func server() {
    // Accept a TCP connection
    conn, err := listener.Accept()
    if err != nil {
        panic(err)
    }

    // Setup server side of emux
    cfg := emux.DefaultConfig()
    cfg.CipherType = "aes-128-gcm"
    cfg.Password = "password"
    session, err := emux.Server(conn, cfg)
    if err != nil {
        panic(err)
    }

    // Accept a stream
    stream, err := session.Accept()
    if err != nil {
        panic(err)
    }

    // Listen for a message
    buf := make([]byte, 4)
    stream.Read(buf)
}

```

Use Pool to create Stream

```go
func client() {
    // Create Config of emux
    cfg := emux.DefaultConfig()
    cfg.CipherType = "aes-128-gcm"
    cfg.Password = "password"

    // Create Pool
    pool, err := emux.NewPool("127.0.0.1:80", cfg, 10)
    if err != nil {
        panic(err)
    }

    // Open a new stream
    stream, err := pool.Open()
    if err != nil {
        panic(err)
    }

    // Stream implements net.Conn
    stream.Write([]byte("ping"))
}

```
