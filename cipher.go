package emux

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/binary"
	"errors"
)

const SALT = "cecb7328135c3dede13d08b06aa344c5"

var (
	_zerononce           [128]byte
	UnkonwnCipherTypeErr = errors.New("Unknown cipher type")
)

type CipherConfig struct {
	KeySize int
}

var CIPHER_TYPE_MAP = map[string]CipherConfig{
	"aes-128-gcm": {16},
	"aes-192-gcm": {24},
	"aes-256-gcm": {32},
}

type Cipher struct {
	cipherType string
	aead       cipher.AEAD
	nonce      []byte
	nonceSize  int
	overhead   int
}

func NewBlockCipher(cipherType string, pass []byte, streamID uint32) (*Cipher, error) {
	cfg, have := CIPHER_TYPE_MAP[cipherType]
	if !have {
		return nil, UnkonwnCipherTypeErr
	}
	block, err := aes.NewCipher(pass[:cfg.KeySize])
	if err != nil {
		return nil, err
	}
	aead, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonce := make([]byte, aead.NonceSize())
	binary.BigEndian.PutUint32(nonce, streamID)
	cipher := &Cipher{
		cipherType: cipherType,
		aead:       aead,
		nonce:      nonce,
		nonceSize:  aead.NonceSize(),
		overhead:   aead.Overhead(),
	}
	return cipher, nil
}

func (c *Cipher) Overhead() int {
	return c.overhead
}

func (c *Cipher) Encrypt(data []byte) []byte {
	ret := c.aead.Seal(nil, c.nonce, data, nil)
	return ret
}

func (c *Cipher) Decrypt(data []byte) ([]byte, error) {
	ret, err := c.aead.Open(nil, c.nonce, data, nil)
	return ret, err
}
