package emux

import (
	"net"
	"sync"
	"sync/atomic"
)

type Pool struct {
	size       int
	rr         uint32
	sessions   []*Session
	targetAddr *net.TCPAddr
	config     *Config
	lock       sync.Mutex
}

// Create new connection pool
func NewPool(addr string, config *Config, size int) (*Pool, error) {
	taddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, err
	}
	return &Pool{
		size:       size,
		rr:         0,
		targetAddr: taddr,
		sessions:   make([]*Session, size),
		config:     config,
	}, nil
}

// Return pool size
func (p *Pool) Size() int {
	return p.size
}

// Create a session
func (p *Pool) createSession() (*Session, error) {
	conn, err := net.DialTCP("tcp", nil, p.targetAddr)
	if err != nil {
		return nil, err
	}
	session, err := Client(conn, p.config)
	if err != nil {
		conn.Close()
		return nil, err
	}
	return session, nil
}

// Get a session from pool
func (p *Pool) TakeSession() (*Session, error) {
	_, sess, err := p.takeSession(true, 0)
	return sess, err
}

// Get session from pool. If toked session's stream id exhausted,
// it will create new session
func (p *Pool) takeSession(autoID bool, id uint32) (uint32, *Session, error) {
	if autoID {
		id = atomic.AddUint32(&p.rr, 1) % uint32(p.size)
	}
	sess := p.sessions[id]
	if sess == nil || sess.IsClosed() {
		p.lock.Lock()
		p.sessions[id] = nil
		p.lock.Unlock()
		nsess, err := p.createSession()
		if err != nil {
			return id, nil, err
		}
		needClose := false
		p.lock.Lock()
		if p.sessions[id] == nil {
			p.sessions[id] = nsess
		} else {
			needClose = true
		}
		p.lock.Unlock()
		if needClose {
			nsess.Close()
		}
	} else if sess.StreamIDExhausted() {
		// Process session stream ID exhausted
		nsess, err := p.createSession()
		if err != nil {
			return id, nil, err
		}
		needClose := false
		p.lock.Lock()
		if p.sessions[id] != sess {
			p.sessions[id] = nsess
		} else {
			needClose = true
		}
		p.lock.Unlock()
		if needClose {
			nsess.Close()
		}
	}
	return id, p.sessions[id], nil
}

// Take a Session and then create a Stream from Session toked.
// It will create a new Session when stream id exhausted, and
// retry create Stream from new created Session.
func (p *Pool) OpenStream() (*Stream, error) {
	id, sess, err := p.takeSession(true, 0)
	if err != nil {
		return nil, err
	}
	stream, err := sess.OpenStream()
	if err != nil {
		if sess.StreamIDExhausted() {
			_, sess, err := p.takeSession(false, id)
			if err != nil {
				return nil, err
			}
			return sess.OpenStream()
		} else {
			return nil, err
		}
	}
	return stream, nil
}

// Alias for OpenStream
func (p *Pool) Open() (*Stream, error) {
	return p.OpenStream()
}

func (p *Pool) Close() {
	for id := range p.sessions {
		sess := p.sessions[id]
		if sess == nil || sess.IsClosed() {
			continue
		}
		sess.Close()
	}
}
